'use strict';

let sleepNumber = 1000;
let chatBox = document.getElementById('chat');

function receiveMessage(event) {
    chatBox.innerHTML +=
        '&lt;You&gt; ' + JSON.parse(event.data).message + '<br>';

    JSON.parse(event.data).replies.forEach(function (reply) {
        setTimeout(function () {
            chatBox.innerHTML += '&lt;Bot&gt; ' + reply + '<br>';
        }, sleepNumber);

        sleepNumber = getNewNumber(sleepNumber);
    });

    scrollDown();
}

function sendMessage(ws) {
    let message = document.getElementById('message');

    ws.send(JSON.stringify({message: message.value}));

    // Clear message box
    message.value = '';

    scrollDown();
}

function sayHi() {
    setTimeout(function () {
        chatBox.innerHTML +=
            '&lt;Bot&gt; Greetings netizen 👋 what can I help you with?<br>';
    }, sleepNumber);
}

function getNewNumber(oldNumber) {
    return (1000 >= oldNumber)
        ? sleepNumber + Math.floor(Math.random() * 2000) : 1000;
}

function scrollDown() {
    setTimeout(function () {
        chatBox.scrollTop = chatBox.scrollHeight;
    }, sleepNumber + 100);
}
