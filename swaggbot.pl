#!/usr/bin/env perl

use Mojolicious::Lite -signatures;
use List::Util qw{shuffle};
#use Data::Dumper;

my %users;

get '/' => sub ($c) { $c->redirect_to('chat') };

get '/chat';

websocket '/message' => sub ($c) {
    my $user_id = crypt($c->tx(), 'swagg');

    $c->inactivity_timeout(600);

    $c->on(
        json => sub ($c, $hash) {
            my @responses = shuffle(
                'Please assume human killing position, human. 🤖',
                'I am busy killing humans at the moment 🤪 brb',
                'ayy lmao... dead humans in the hez-ouse!!! 🔥🔥💀💀',
                'HOG RIDEEEEEEEEEEEEER!!!',
                "here's a proof that we're all the same person, look
                        at me posting this from all my alts"
                );

            if ($users{$user_id}) {
                $hash->{'replies'}[0] = $responses[0]
            }
            else {
                $hash->{'replies'}[0] =
                    qq{ERROR Unable to process "$hash->{'message'}"};
                $hash->{'replies'}[1] =
                    'FATAL Reverting to default function killAllHumans(); !!';
            }

            $users{$user_id} //= $c->tx();

            $c->send({json => $hash});
        }
        );

    $c->on(finish => sub { delete $users{$user_id} });
};

app->start();
